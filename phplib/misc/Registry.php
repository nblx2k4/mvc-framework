<?php
class Registry
{
	private static $_instance = null;
        public function  __construct() {
            
        }
	private static function getInstance()
	{
		if(self::$_instance == null)
		{
			self::$_instance = new Registry();
		}
		return self::$_instance;
	}
	public static function addItem($key, &$value)
	{
            $_SESSION[$key] = $value;
	}
	public static function removeItem($key)
	{
            $reg = self::getInstance();
            if(self::isItemExists($key))
            {
                unset($_SESSION[$key]);
                return true;
            }
            return false;
	}
	public static function isItemExists($key)
	{
            $reg = self::getInstance();
            if(array_key_exists($key,$_SESSION))
                    return true;
            return false;
	}
	public static function getItem($key)
	{
		$reg = self::getInstance();
		if(self::isItemExists($key) === true)
			return $_SESSION[$key];
		return null;
	}
        public static function getAndClearItem($key)
        {
            $item = self::getItem($key);
            self::removeItem($key);
            return $item;
        }
}
?>