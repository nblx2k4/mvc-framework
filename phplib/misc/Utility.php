<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Utility
{
    public static function convertToArray($objArray)
    {
        if(!is_array($objArray))
            throw new Exception("The parameter is not an array of obects.");
        $rootArray = array();
        foreach($objArray as $object)
        {
            if(!is_object($object))
                throw new Exception("All the array elements must be objects.");

            $objVars = get_object_vars($object);
            $rootArray[] = $objVars;
        }
        return $rootArray;
    }
    public static function writeLog($str)
    {
        $fp = fopen("log.txt","a+");
        fseek($fp, filesize("log.txt")-1);
        fwrite($fp,date("d/m/Y H:i:s")." : ".$str);
        fclose($fp);
    }
    public static function generateKey()
    {
        $strArray = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $keys = array(7,4,6,15);
        $key = "";
        for($i=0;$i<count($keys);$i++)
        {
            for($j=0;$j<$keys[$i];$j++)
            {
                $key .= $strArray[rand(0,  strlen($strArray)-1)];
            }
            if(($i+1) != count($keys))
                $key .= "-";
        }
        return $key;
    }
}
