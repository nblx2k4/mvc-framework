<?php
require_once("RL/TemplateProvider.php");
require_once("misc/ClassLoader.php");

class FrontControllerAction
{
	protected $_request = null;
	protected $_template = null;
	private $_actionName = "";
	private $_controllerName = "";
	public function __construct()
	{
            $this->_request = new HttpRequest();
            $this->_template = new TemplateProvider();
            ClassLoader::load("DAL_CommandExecutor");
            ClassLoader::load("misc_Utility");
	}
	public function getTemplate()
	{
		return $this->_template;
	}
	public function setActionName($actionName)
	{
		$this->_actionName = $actionName;
	}
	public function getActionName()
	{
		return $this->_actionName;
	}
	public function setControllerName($controllerName)
	{
		$this->_controllerName = $controllerName;
	}
	public function getControllerName()
	{
		return $this->_controllerName;
	}
	public function setTemplateDirectory($dir)
	{
		$this->_template->setTemplateDirectory($dir."/".strtolower($this->_controllerName));
	}
	public function parse()
	{
                $this->_request->addHeader("Content-type","text/html; charset=UTF-8");
		$this->_template->setTemplateFile($this->_actionName);
		$this->_template->parse();
	}
	public function getRequest()
	{
		return $this->_request;
	}
	public function init(){}
	public function preDispatch(){}
}
