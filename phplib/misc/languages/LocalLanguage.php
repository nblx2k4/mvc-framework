<?php
define('CONFIG_LANGUAGE_FILE_INCLUDED',true);
class LocalLanguage
{
	static function getErrorMessage($message,$lang = 'en_US')
	{
		$langsDirectory = dirname(__FILE__);
		$langFile = $langsDirectory."/languages/{$lang}.xml";
		if(!file_exists($langsDirectory."/languages/{$lang}.xml"))
			throw new Exception("Cannot find the language xml file.");
		$xml = simplexml_load_file($langFile);
		foreach($xml->messages->message as $msg)
		{
			if($msg->key == $message)
			{
				return $msg->value;
			}
		}
		return null;
	}
}
?>