<?php
final class ClassLoader
{
	public static function load($className,$extension = "php", $directory = "")
	{
		$includePath = Registry::getItem("INCLUDE_PATH");
		if($directory == "" && isset($includePath))
			$directory = Registry::getItem("INCLUDE_PATH");
		$classPath = $directory."/".str_replace("_","/",$className).".{$extension}";

		if(!is_dir($directory))
			throw new Exception("Invalid class path.");
		
		if(!is_file($classPath))
			throw new Exception("The class file {$className} don't exist.");
			
		require_once($classPath);
	}
}
?>