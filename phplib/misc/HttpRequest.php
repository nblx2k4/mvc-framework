<?php
class HttpRequest
{
	public function isPost()
	{
		if($_SERVER["REQUEST_METHOD"] == "POST")
			return true;
		return false;
	}
	public function isGet()
	{
		if($_SERVER["REQUEST_METHOD"] == "GET")
			return true;
		return false;
	}
	public function getParam($key)
	{
		if(!is_string($key))
			return false;
		
		if(!isset($_GET[$key]))
			return false;
		
		return stripslashes($_GET[$key]);
	}
	public function getPost($key)
	{
		if(!is_string($key))
			return false;
		
		if(!isset($_POST[$key]))
			return false;
		
		return stripslashes($_POST[$key]);
	}
	public function getUrl()
	{
		$url = "http".(($_SERVER["SERVER_PORT"]==443)?"s":"")."://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"]);
		return $url;
	}
	public function redirect($url)
	{
		if(headers_sent())
			throw new Exception("cannot redirect because the header is already sent.");
		
		header("Location:".$url);
	}
	public function addHeader($key,$value = null)
	{
		if(headers_sent())
			throw new Exception("cannot add header because the header is already sent.");
		
		if(isset($value))
			header("{$key}:{$value}");
		else
			header("{$key}");
	}
}
?>