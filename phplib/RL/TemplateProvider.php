<?php
require_once("template/Smarty.class.php");
class TemplateProvider
{
	private $_templateFile = "";
	private $_templateFilePrefix = "html";
	private $_templeDirectory = "";
	private $_templateLite = null;
	private $_useCommonTemplate = false;
	private $_headerFile = "";
	private $_footerFile = "";
	private $_sections = array();
        
	public function __construct()
	{
		$this->_templateLite = new Smarty();
                $this->_templateLite->register_modifier("enc", "utf8_encode");
	}
	public function setTemplateFile($actionName)
	{
		$this->_templateFile = $actionName.".".$this->_templateFilePrefix;
	}
	public function parse($file = null)
	{
		if($file == null)
			$file = $this->_templateFile;
		
		if(!file_exists($this->_templeDirectory."/raw/".$file))
			throw new Exception("The template file {$file} was not found.");
			
		if(!is_dir($this->_templeDirectory."/raw"))
			throw new Exception("The raw template directory don't exists.");
		
		if(!is_dir($this->_templeDirectory."/compiled"))
			throw new Exception("The compiled template directory don't exists.");
		
		$this->_templateLite->compile_dir = $this->_templeDirectory."/compiled/";
		$this->_templateLite->template_dir = $this->_templeDirectory."/raw/";
		
		$this->_templateLite->display($file);
	}
	public function setVariable($key, $value = null)
	{
		$this->_templateLite->assign($key, $value);
	}
	public function setTemplateDirectory($directory)
	{
		$this->_templeDirectory = $directory;
	}
	public function setCommonTemplate($flag)
	{
		$this->_useCommonTemplate = $flag;
	}
	public function setHeaderFile($fileName,$headerSection)
	{
		$this->_headerFile = $fileName;
		$this->_sections["HEADER_SECTION"] = $headerSection;
	}
	public function setFooterFile($fileName,$footerSection)
	{
		$this->_footerFile = $fileName;
		$this->_sections["FOOTER_SECTION"] = $headerSection;
	}
}
?>