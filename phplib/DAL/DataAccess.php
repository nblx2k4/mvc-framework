<?php
require_once("DBAdapter.php");
abstract class DataAccess
{
    private $_tableFields = array();
    protected $_adapter = null;
    private $_primaryKey = NULL;
    protected $_name = "";
    public function getFieldsTable()
    {
        return $this->_tableFields;
    }
    protected abstract function selectRaw($query,$where=null, $order=null, $group=null, $limit=null, $stack=null);
    protected abstract function select($where=null, $order=null, $group=null, $limit=null, $stack=null);
    protected abstract function selectJoin($fields,$tables,$where=null,$order=null,$group=null,$limit=null,$stack=null);
    protected abstract function insert();
    protected abstract function update();
    protected abstract function delete($where=NULL);
    protected function getSchema()
    {
        $query = "describe `{$this->_name}`";
        $result = $this->_adapter->query($query);
        if($result != NULL)
        {
            while($row = $result->fetchRow())
            {
                $this->{$row->Field} = NULL;
                $this->_tableFields[] = $row->Field;
                if(isset($row->Key) && $row->Key == "PRI")
                  $this->_primaryKey = $row->Field;
            }
        }
    }
    protected function getAdapter()
    {
        return $this->_adapter;
    }
    protected function executeQuery($query)
    {
        return $this->_adapter->query($query);
    }
    protected function getPrimaryKeyField()
    {
        return $this->_primaryKey;
    }
}