<?php
require_once("DBI.class.php");

class DBAdapter
{
	private static $_instance = NULL;
	private $_host = "";
	private $_dbname = "";
	private $_user = "";
	private $_pass = "";
	private $_dbInterface;
	private function __construct()
	{
		$dbUrl = "mysql://{$this->_user}:{$this->_pass}@{$this->_host}/{$this->_dbname}";
		$this->_dbInterface = new DBI($dbUrl);
		if($this->_dbInterface->isError())
			throw new Exception("Cannot connect to the database server. \nDetail : ".$this->_dbInterface->getError());
	}
	public static function getInstance()
	{
		if(is_null(self::$_instance) == TRUE)
		{
			self::$_instance = new DBAdapter();
		}
		return self::$_instance;
	}
	public function getDBInterface()
	{
		return $this->_dbInterface;
	}
	function __destruct()
	{
		if($this->_dbInterface->isConnected())
			$this->_dbInterface->disconnect();
	}
}