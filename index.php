<?php
//make php errors and notices visible in the output page
error_reporting(E_ALL|E_WARNING);
@session_start();
$rootDirectory = dirname(__FILE__);
$documentRoot = $rootDirectory!="/"?$rootDirectory:"";
$PHPLIB_DIR = $documentRoot . '/phplib';
$PEAR_LIB_DIR = $documentRoot . '/PEAR';
$templatesPath = $documentRoot . 'templates';

ini_set("include_path",PATH_SEPARATOR.$PHPLIB_DIR.PATH_SEPARATOR.$PEAR_LIB_DIR.PATH_SEPARATOR.ini_get("include_path"));

include("RL/FrontController.php");
$front = FrontController::getInstance();
$front->setIncludePath($PHPLIB_DIR);
$front->setIncludePath($documentRoot."/phplib");
$front->setAbsolutePath($documentRoot."/");
$front->setTemplateDirectory($documentRoot."/templates");
try
{
	$front->dispatch();
}catch(Exception $ex)
{
	echo $ex->getMessage();
	/*if(!headers_sent())
		header("Location:404.html");
	else
	{
		include("404.html");
	}*/
}

?>